package middleware

import (
	"log"

	"github.com/labstack/echo/v4"
)

func LogMiddlewarware(next echo.HandlerFunc) echo.HandlerFunc {

	log.Println(" Mypost it บันทึกแล้ว")

	return func(ctx echo.Context) error {

		return next(ctx)

	}
}
