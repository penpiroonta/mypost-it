package models

type Postit struct {
	ID    int    `json:"id"`
	Story string `json:"story"`
	Date  string `json:"date"`
}

func (s Postit) TableName() string {

	return "mypostit"

}
