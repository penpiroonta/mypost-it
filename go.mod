module postit

go 1.16

require (
	
	github.com/labstack/echo/v4 v4.9.1
	gorm.io/driver/mysql v1.4.4
	gorm.io/gorm v1.24.2
)
