package repositories

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var ConnectionPool *gorm.DB

func InnitTialDB() {

	dsn := "root:@tcp(127.0.0.1:3306)/resful?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		panic(err)
	}

	ConnectionPool = db

}
