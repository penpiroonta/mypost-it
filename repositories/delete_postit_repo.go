package repositories

import (
	"postit/models"
)

func DeleteShoptwoRepo(number int) error {

	return ConnectionPool.Where("number=?", number).Delete(models.Postit{}).Error

}
