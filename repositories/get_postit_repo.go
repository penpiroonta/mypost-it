package repositories

import "postit/models"

func GetPostitRepo() []models.Postit {
	postit := new([]models.Postit)

	ConnectionPool.Find(&postit)
	return *postit

}
