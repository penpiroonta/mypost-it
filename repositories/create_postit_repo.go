package repositories

import "postit/models"

func CreatePostitRepo(postit models.Postit) models.Postit {

	ConnectionPool.Create(&postit)
	return postit

}
