package repositories

import "postit/models"

func UpdatePostitRepo(postit models.Postit) error {

	return ConnectionPool.Debug().Where("date=?", postit.Date).Updates(&postit).Error

}
