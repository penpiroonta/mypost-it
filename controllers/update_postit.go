package controllers

import (
	"postit/models"
	"postit/repositories"

	"github.com/labstack/echo/v4"
)

func UpdatePostitColltrollers(ctx echo.Context) error {

	date := ctx.Param("date")

	postit := new(models.Postit)

	ctx.Bind(&postit)

	postit.Date = date

	err := repositories.UpdatePostitRepo(*postit)

	if err != nil {
		return ctx.JSON(400, map[string]interface{}{
			"error": err.Error(),
		})
	}

	return ctx.JSON(200, map[string]interface{}{
		"data": "susscess",
	})

}
