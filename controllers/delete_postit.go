package controllers

import (
	"postit/repositories"
	"strconv"

	"github.com/labstack/echo/v4"
)

func DeletePostitControllers(ctx echo.Context) error {

	numberString := ctx.Param("number")

	number, _ := strconv.Atoi(numberString)
	err := repositories.DeleteShoptwoRepo(number)

	if err != nil {
		ctx.JSON(400, map[string]interface{}{
			"error": err.Error(),
		})
	}
	return ctx.JSON(200, map[string]interface{}{
		"data": "susscess",
	})

}
