package controllers

import (
	"postit/repositories"

	"github.com/labstack/echo/v4"
)

func GetPostitControllers(ctx echo.Context) error {

	postit := repositories.GetPostitRepo()
	return ctx.JSON(200, map[string]interface{}{
		"data": postit,
	})

}
