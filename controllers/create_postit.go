package controllers

import (
	"postit/models"
	"postit/repositories"

	"github.com/labstack/echo/v4"
)

func CreatePostitController(ctx echo.Context) error {

	postitStory := new(models.Postit)

	ctx.Bind(&postitStory)

	if postitStory.Story == "" {
		return ctx.JSON(400, map[string]interface{}{
			"error": "No story",
		})
	}

	postit := repositories.CreatePostitRepo(*postitStory)

	return ctx.JSON(200, map[string]interface{}{
		"data": postit,
	})

}
