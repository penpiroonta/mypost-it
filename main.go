package main

import (
	"postit/controllers"
	"postit/middleware"

	"postit/repositories"

	"github.com/labstack/echo/v4"
)

func main() {

	repositories.InnitTialDB()
	db, _ := repositories.ConnectionPool.DB()
	defer db.Close()

	e := echo.New()

	e.POST("/postit", controllers.CreatePostitController, middleware.LogMiddlewarware)
	e.GET("/postit", controllers.GetPostitControllers)
	e.PUT("/postit/:date", controllers.UpdatePostitColltrollers)
	e.DELETE("/postit/:number", controllers.DeletePostitControllers)

	e.Logger.Fatal(e.Start(":1323"))

}
